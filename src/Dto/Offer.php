<?php

/*
 * This file is part of the Ekwateur Test package.
 *
 * (c) Sana Znaidi <znaidisana@yahoo.fr>
 *
 */

namespace App\Dto;

/**
 * Get offer details.
 *
 * @author Sana Znaidi <znaidisana@yahoo.fr>
 */

class Offer
{
    /**
     * @var string
     */
    private $offerType;

    /**
     * @var string
     */
    private $offerName;

    /**
     * @var string
     */
    private $offerDescription;

    /**
     * @var array
     */
    private $validPromoCodeList;

    /**
     * Offer Constructor
     * 
     * @param string $offerType
     * @param string $offerName
     * @param string $offerDescription
     * @param array $validPromoCodeList
     */
    public function __construct(string $offerType, string $offerName, string $offerDescription, array $validPromoCodeList)
    {
        $this->offerType = $offerType;
        $this->offerName = $offerName;
        $this->offerDescription = $offerDescription;
        $this->validPromoCodeList = $validPromoCodeList;
    }

    /**
     * Sets an offer type info.
     * 
     * @return $this
     */
    public function setOfferType($offerType)
    {
        $this->offerType = $offerType;
        return $this;
    }

    /**
     * @return string
     */
    public function getOfferType()
    {
        return $this->offerType;
    }

    /**
     * Sets an offer name info.
     * 
     * @return $this
     */
    public function setOfferName($offerName)
    {
        $this->offerName = $offerName;
        return $this;
    }

    /**
     * @return string
     */
    public function getOfferName()
    {
        return $this->offerName;
    }

    /**
     * Sets an offer description info.
     * 
     * @return $this
     */
    public function setOfferDescription($endDate)
    {
        $this->offerDescription = $offerDescription;
        return $this;
    }

    /**
     * @return string
     */
    public function getOfferDescription()
    {
        return $this->offerDescription;
    }

    /**
     * Sets a Valid promoCode list info.
     * 
     * @return $this
     */
    public function setValidPromoCodeList($validPromoCodeList)
    {
        $this->validPromoCodeList = $validPromoCodeList;
        return $this;
    }

    /**
     * @return array
     */
    public function getValidPromoCodeList()
    {
        return $this->validPromoCodeList;
    }
    
}