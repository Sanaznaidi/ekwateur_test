<?php

/*
 * This file is part of the Ekwateur Test package.
 *
 * (c) Sana Znaidi <znaidisana@yahoo.fr>
 *
 */

namespace App\Dto;

/**
 * Get promo code details.
 *
 * @author Sana Znaidi <znaidisana@yahoo.fr>
 */

class PromoCode
{
    /**
     * @var string
     */
    private $code;

    /**
     * @var float
     */
    private $discountValue;

    /**
     * @var string
     */
    protected $endDate;

    /**
     * @var array
     */
    private $offersArray = array();

    public function __construct(string $code, float $discountValue, string $endDate, array $offersArray)
    {
        $this->code = $code;
        $this->discountValue = $discountValue;
        $this->endDate = $endDate;
        $this->offersArray = $offersArray;
    }

    /**
     * Sets a promo code info.
     * 
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }
    
    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Sets a discount value info.
     * 
     * @return $this
     */
    public function setDiscountValue($discountValue)
    {
        $this->discountValue = $discountValue;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscountValue()
    {
        return $this->discountValue;
    }

    /**
     * Sets an end date info.
     * 
     * @return $this
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Check if promo code is not expired
     * 
     * @return boolean
     */
    public function checkDateValidation()
    {
        $formattedEndDate = new \DateTime($this->endDate);
        $currentDate = new \DateTime("now");
        return $formattedEndDate >= $currentDate;
    }

    /**
     * Get Offers list by promo code
     * 
     * @return array
     */
    public function getOffersList()
    {
        $offersList = [];
        foreach ($this->offersArray as $offer) {
            if (in_array($this->code, $offer->getValidPromoCodeList())) {
                $offersList[] = array('name' => $offer->getOfferName() , 'type' => $offer->getOfferType());
            }
        }
        return $offersList;
    }

}