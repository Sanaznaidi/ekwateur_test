<?php

/*
 * This file is part of the Ekwateur Test package.
 *
 * (c) Sana Znaidi <znaidisana@yahoo.fr>
 *
 */

namespace App\Command;

use App\Dto\PromoCode;
use App\Dto\Offer;
use App\Service\MockApi;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpClient\HttpClient;

/**
 * A console command that get valid promoCode with associated offers.
 *
 * To use this command, open a terminal window, enter into your project
 * directory and execute the following:
 *
 *     $ php bin/console promo-code:validate MON_CODE_PROMO_A_TESTER
 *
 * Check out the code of the src/Command/PromoCodeValidateCommand.php file for
 * the full explanation about Symfony commands.
 *
 * See https://symfony.com/doc/current/console.html
 *
 * @author Sana Znaidi <znaidisana@yahoo.fr>
 */
class PromoCodeValidateCommand extends Command
{   
    // to make your command lazily loaded, configure the $defaultName static property,
    // so it will be instantiated only when the command is actually called.
    protected static $defaultName = 'promo-code:validate';
    // command description
    protected static $defaultDescription = 'Get Offers by promo code';

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * Configuration of the command.
     * 
     * @return integer
     */
    protected function configure()
    {
        $this->setDescription(self::$defaultDescription)
            ->addArgument('promoCode', InputArgument::OPTIONAL, 'Promo code to give');
    }

    /**
     * This optional method is the first one executed for a command after configure()
     * and is useful to initialize properties based on the input arguments and options.
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * Execute the command.
     * 
     * @param Object $input  Object Input.
     * @param Object $output Object Output.
     * 
     * @return integer
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $promoCode = $input->getArgument('promoCode');
            if (empty($promoCode)) {
                $this->io->error('Mising argument promoCode');
                return Command::FAILURE;
            } else {
                $client = HttpClient::create();
                $mockApi = new MockApi($client);
                $promoCodeList = $mockApi->getPromoCodeList();
                $promoCodeObject = $this->getValidPromoCode($promoCodeList, $promoCode);
                // check if this promo code has offers
                if (count($promoCodeObject->getOffersList()) > 0) {
                    $result = array(
                        'promoCode' => $promoCodeObject->getCode(), 
                        'endDate' => $promoCodeObject->getEndDate(),
                        'discountValue' => $promoCodeObject->getDiscountValue(),
                        'compatibleOfferList' => $promoCodeObject->getOffersList()
                    );
                    $result =  json_encode($result, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                    $this->createJsonFile($result);
                } else {
                    $this->io->success('There is no offers for this promoCode : ' . $promoCode);
                }
                return Command::SUCCESS;
            }
        } catch (\Throwable $th) {
            $this->io->error($th->getMessage());
            return Command::FAILURE;
        }
    }

    /**
     * Get Valid PromoCode by code after checking promoCode endDate 
     * 
     * @param Array $promoCodeData  Collection of \PromoCode.
     * @param string $promoCode.
     * 
     * @return \PromoCode
     */
    protected function getValidPromoCode($promoCodeData, $promoCode): PromoCode
    {
        $isExist = FALSE;
        foreach ($promoCodeData as $promoCodeObject) {
            if ($promoCodeObject->getCode() === $promoCode) {
                if (!$promoCodeObject->checkDateValidation()) {
                    throw new \Exception('Promo Code is expired');
                }
                $object = $promoCodeObject;
                $isExist = TRUE;
            }
        }
        if (!$isExist) {
            throw new \Exception('Promo Code does not exit');
        }
        return $object;
    }


    /**
     * Create json file from promoCode json response
     * 
     * @param Object $jsonResponse
     * 
     * @return io response
     */
    private function createJsonFile($jsonResponse)
    {
        try {
            $filesystem = new Filesystem();
            $dirPath = sys_get_temp_dir().'/'.'ekwateur';
            $filesystem->mkdir($dirPath , 0775);
            $filePath = $dirPath . "/validPromoCode.json";
            $filesystem->remove($filePath);
            if (!$filesystem->exists($filePath))
            {
                $filesystem->touch($filePath);
                $filesystem->chmod($filePath, 0777);
                $filesystem->dumpFile($filePath, $jsonResponse);
            }
            $this->io->success('Your file path ' . $filePath);
        } catch (IOExceptionInterface $exception) {
            echo "Error creating file at". $exception->getPath();
        }
    }

}
