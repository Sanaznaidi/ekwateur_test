<?php
/*
 * This file is part of the Ekwateur Test package.
 *
 * (c) Sana Znaidi <znaidisana@yahoo.fr>
 *
 */

namespace App\Service;

use App\Dto\PromoCode;
use App\Dto\Offer;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Provides flexible methods for requesting ekwatest api and formatting content.
 *
 * @author Sana Znaidi <znaidisana@yahoo.fr>
 */
class MockApi
{
    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * Mock API URL
     */
    private const MOCk_API_URL = 'https://601025826c21e10017050013.mockapi.io/ekwatest/';

    /**
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * Request Mock Api to get content list (PromoCode or Offer)
     * @return array
     */
    public function getPromoCodeList(): array
    {   
        // get Offers Information
        $offers = $this->getOffers();
        // get PromoCode Infomation
        $promoCodes = $this->getPromoCode($offers);
        return $promoCodes;
    }

    /**
     * Request Mock Api to get content list (PromoCode or Offer)
     * @return array
     */
    private function callApi($urlPath)
    {
        $content = [];
        $response = $this->client->request(
            'GET',
            self::MOCk_API_URL . $urlPath . 'List'
        );

        // check code response to throw execption
        if (200 !== $response->getStatusCode()) {
            throw new \Exception('Can not access to ekwateur api');
        }
        $content = $response->getContent();
        $content = $response->toArray();
        return $content;
    }

    /**
     * Request Mock Api to get offer list and format content to an array of offers object
     * 
     * @return array
     */
    private function getOffers()
    {
        $offersArray = $this->callApi('offer');
        if (empty($offersArray)) {
            throw new \Exception("There is no offers");
        }
        $offers = [];
        foreach ($offersArray as $offer) {
            $OfferObject = new Offer($offer['offerType'], $offer['offerName'], $offer['offerDescription'], $offer['validPromoCodeList']);
            $offers[] = $OfferObject;
        }
        return $offers;
    }

    /**
     * Request Mock Api to get promo list and format content to an array of promocode object
     * 
     * @return array
     */
    private function getPromoCode($offers)
    {
        $promoCodeArray = $this->callApi('promoCode');
        if (empty($promoCodeArray)) {
            throw new \Exception("There is no promoCode");
        }
        $promoCodes = [];
        foreach ($promoCodeArray as $promoCode) {
            $promoCodeobject = new PromoCode($promoCode['code'], $promoCode['discountValue'], $promoCode['endDate'], $offers);
            $promoCodes[] = $promoCodeobject;
        }
        return $promoCodes;
    }

}