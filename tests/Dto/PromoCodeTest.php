<?php

namespace App\Tests\Dto;

use App\Dto\PromoCode;
use App\Dto\Offer;
use PHPUnit\Framework\TestCase;

class PromoCodeTest extends TestCase
{
    public function testPromoCodeCreate()
    {
        $promoCode = new PromoCode('ClocloCode', 1.77, '2022-01-07', 
            array(
                new Offer('OfferType1', 'Offer1', 'Offer1 Description', array('ClocloCode', 'promoCode2')),
                new Offer('OfferType2', 'Offer2', 'Offer1 Description', array('promoCode1', 'promoCode2')),
                new Offer('OfferType3', 'Offer3', 'Offer3 Description', array('ClocloCode', 'promoCode3'))
            )
        );
        $offerArray = [
            [ 
                'name' => 'Offer1',
                'type' => 'OfferType1'
            ],
            [ 
                'name' => 'Offer3',
                'type' => 'OfferType3'
            ]
        ];
        $this->assertEquals("ClocloCode", $promoCode->getCode());
        $this->assertEquals(1.77, $promoCode->getDiscountValue());
        $this->assertEquals("2022-01-07", $promoCode->getEndDate());
        $this->assertTrue($this->arrays_are_similar($offerArray[0], $promoCode->getOffersList()[0]));
        $this->assertTrue($this->arrays_are_similar($offerArray[1], $promoCode->getOffersList()[1]));
    }

    
    /**
     * @covers ::checkDateValidation
     */
    public function testCheckDateValidationOk()
    {
        $promoCode = new PromoCode('ClocloCode', 1.77, '2022-01-07', []);
        $checkDateValidation =  $promoCode->checkDateValidation();
        $this->assertTrue($checkDateValidation);

    }

    /**
     * @covers ::checkDateValidation
     */
    public function testCheckDateValidationKO()
    {
        $promoCode = new PromoCode('ClocloCode', 1.77, '2020-01-07', []);
        $checkDateValidation =  $promoCode->checkDateValidation();
        $this->assertFalse($checkDateValidation);
    }


    /**
     * Determine if two associative arrays are similar
     *
     * Both arrays must have the same indexes with identical values
     * without respect to key ordering 
     * 
     * @param array $a
     * @param array $b
     * @return bool
     */
    function arrays_are_similar($a, $b) {
        if (count(array_diff_assoc($a, $b))) {
            return false;
        }
        foreach ($a as $k => $v) {
            if ($v !== $b[$k]) {
                return false;
            }
        }
        return true;
  }
  
}