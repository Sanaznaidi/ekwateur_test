<?php

namespace App\Tests\Dto;

use App\Dto\Offer;
use PHPUnit\Framework\TestCase;

class OfferTest extends TestCase
{
    public function testOfferCreate()
    {
        $offer = new Offer('ClocloType', 'ClocloName', 'Cloclo Description', array('promoCode1', 'promoCode2'));

        $this->assertEquals("ClocloType", $offer->getOfferType());
        $this->assertEquals("ClocloName", $offer->getOfferName());
        $this->assertEquals("Cloclo Description", $offer->getOfferDescription());
        $this->assertTrue($this->arrays_are_similar(array('promoCode1', 'promoCode2'), $offer->getValidPromoCodeList()));
    }

    
    /**
     * Determine if two associative arrays are similar
     *
     * Both arrays must have the same indexes with identical values
     * without respect to key ordering 
     * 
     * @param array $a
     * @param array $b
     * @return bool
     */
    function arrays_are_similar($a, $b) {
        if (count(array_diff_assoc($a, $b))) {
            return false;
        }
        foreach($a as $k => $v) {
            if ($v !== $b[$k]) {
                return false;
            }
        }
        return true;
  }

}