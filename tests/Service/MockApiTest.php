<?php

namespace App\Tests\Service;

use App\Service\MockApi;
use App\Dto\Offer;
use PHPUnit\Framework\TestCase;

use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class MockApiTest extends TestCase
{

    public function testGetOffersOk()
    {
        $callbackWasCalled = false;

        $callback = function ($method, $url, $options) use (&$callbackWasCalled) {
            $callbackWasCalled = true;
            return new MockResponse('[
                {
                  "offerType": "GAS",
                  "offerName": "EKWAG2000",
                  "offerDescription": "Une offre incroyable",
                  "validPromoCodeList": [
                    "EKWA_WELCOME",
                    "ALL_2000"
                  ]
                }
              ]');
        };

        $mockedClient = new MockHttpClient($callback);
        $mockApi = new MockApi($mockedClient);
        $getOffers = self::getMethod('getOffers');
        $offers = $getOffers->invokeArgs($mockApi, []);
        $offer = $offers['0'];
        $this->assertEquals(1, count($offers));
        $this->assertEquals("GAS", $offer->getOfferType());
        $this->assertEquals("EKWAG2000", $offer->getOfferName());
        $this->assertEquals("Une offre incroyable", $offer->getOfferDescription());
    }

    public function  testGetOffersEmpty(): void
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('There is no offers');
        $callbackWasCalled = false;

        $callback = function ($method, $url, $options) use (&$callbackWasCalled) {
            $callbackWasCalled = true;
            return new MockResponse('[]');
        };

        $mockedClient = new MockHttpClient($callback);
        $mockApi = new MockApi($mockedClient);
        $getOffers = self::getMethod('getOffers');
        $offers = $getOffers->invokeArgs($mockApi, []);
    }

    public function testGetProductsOk()
    {
        $callbackWasCalled = false;

        $callback = function ($method, $url, $options) use (&$callbackWasCalled) {
            $callbackWasCalled = true;
            return new MockResponse('
                [
                    {
                    "code": "EKWA_WELCOME",
                    "discountValue": 2,
                    "endDate": "2019-10-04"
                    }
              ]');
        };

        $mockedClient = new MockHttpClient($callback);
        $mockApi = new MockApi($mockedClient);
        $getPromoCode = self::getMethod('getPromoCode');
        $promoCodes = $getPromoCode->invokeArgs($mockApi, [
            array(
                new Offer('OfferType1', 'Offer1', 'Offer1 Description', array('EKWA_WELCOME', 'promoCode2')),
                new Offer('OfferType2', 'Offer2', 'Offer1 Description', array('promoCode1', 'promoCode2')),
                new Offer('OfferType3', 'Offer3', 'Offer3 Description', array('EKWA_WELCOME', 'promoCode3'))
            )
        ]);

        $this->assertEquals(1, count($promoCodes));
    }

    public function  testGetProductsEmpty(): void
    {
        $this->expectException('Exception');
        $this->expectExceptionMessage('There is no promoCode');
        $callbackWasCalled = false;

        $callback = function ($method, $url, $options) use (&$callbackWasCalled) {
            $callbackWasCalled = true;
            return new MockResponse('[]');
        };

        $mockedClient = new MockHttpClient($callback);
        $mockApi = new MockApi($mockedClient);
        $getPromoCode = self::getMethod('getPromoCode');
        $promoCodes = $getPromoCode->invokeArgs($mockApi, [array()]);

    }

    protected static function getMethod($name) {
        $class = new \ReflectionClass('App\Service\MockApi');
        $method = $class->getMethod($name);
        $method->setAccessible(true);
        return $method;
      }
}