<?php

namespace App\Tests\Command;

use App\Command\PromoCodeValidateCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class PromoCodeValidateCommandTest extends KernelTestCase
{   

    private $promoCodeData = [
        'promoCode' => 'cloclo'
    ];

    public function testExecutePromoCodeDoesNotExist()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'promoCode' => 'Wouter'
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[ERROR] Promo Code does not exit', $output);

    }

    public function testExecutePromoCodeIsEmpty()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'promoCode' => ''
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[ERROR] Mising argument promoCode', $output);

    }

    public function testExecuteOk()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);

        $command = $application->find('promo-code:validate');
        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'promoCode' => 'BUZZ'
        ]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('[OK] Your file path C:\Users\isabe\AppData\Local\Temp/ekwateur/validPromoCode.json', $output);

    }

}